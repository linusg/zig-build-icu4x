const std = @import("std");

const Allocator = std.mem.Allocator;

fn zigTargetToRustTarget(allocator: Allocator, target: std.Target) Allocator.Error![]const u8 {
    // TODO: rustc only understands a small subset of valid zig target triples,
    //       this is a low effort conversion between the two.
    var target_string = try target.linuxTriple(allocator);
    inline for (&.{
        .{ .needle = "x86-", .replacement = "i686-" },
        .{ .needle = "riscv64-", .replacement = "riscv64gc-" },
        .{ .needle = "linux", .replacement = "unknown-linux" },
        .{ .needle = "macos", .replacement = "apple-darwin" },
        .{ .needle = "windows", .replacement = "pc-windows" },
        .{ .needle = "-wasi-musl", .replacement = "-wasi" },
        .{ .needle = "-none", .replacement = "" },
    }) |entry| {
        target_string = blk: {
            defer allocator.free(target_string);
            break :blk try std.mem.replaceOwned(
                u8,
                allocator,
                target_string,
                entry.needle,
                entry.replacement,
            );
        };
    }
    if (std.StaticStringMap([]const u8).initComptime(.{
        // Rust only seems to have `-msvc` and `-gnullvm` for `aarch64-windows`
        // https://doc.rust-lang.org/rustc/platform-support/pc-windows-gnullvm.html
        .{ "aarch64-pc-windows-gnu", "aarch64-pc-windows-gnullvm" },
        // Special cases for mips
        .{ "mips-unknown-linux-gnueabi", "mips-unknown-linux-gnu" },
        .{ "mipsel-unknown-linux-gnueabi", "mips-unknown-linux-gnu" },
        .{ "mips64-unknown-linux-musl", "mips64-unknown-linux-muslabi64" },
        .{ "mips64el-unknown-linux-musl", "mips64el-unknown-linux-muslabi64" },
    }).get(target_string)) |replacement| {
        allocator.free(target_string);
        return replacement;
    }
    return target_string;
}

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const release = optimize != .Debug;

    const rust_target = zigTargetToRustTarget(b.allocator, target.result) catch @panic("OOM");

    const icu4x = b.dependency("icu4x", .{});

    const run_cargo = std.Build.Step.Run.create(b, "cargo build icu4x");
    run_cargo.expectExitCode(0);
    run_cargo.addArgs(&.{
        "env",
        "RUSTFLAGS=-C panic=abort",
        "cargo",
        "rustc",
        "--package",
        "icu_capi",
        "--crate-type",
        "staticlib",
        "--manifest-path",
    });
    run_cargo.addFileArg(icu4x.path("Cargo.toml"));
    run_cargo.addArgs(&.{
        // Disable the 'logging' and 'simple_logger' features
        "--no-default-features",
        // Re-enable the 'compiled_data', 'default_components', and 'std' features
        // Add the 'experimental' feature
        "--features",
        "icu_capi/compiled_data,icu_capi/experimental,icu_capi/default_components,icu_capi/std",
    });

    if (!target.query.isNative()) {
        run_cargo.addArgs(&.{
            "-Z",
            "build-std=std,panic_abort",
            "-Z",
            "build-std-features=panic_immediate_abort",
            "--target",
            rust_target,
        });
    }
    if (release) {
        run_cargo.addArg("--release");
    }

    const libicu_capi = icu4x.path(
        if (!target.query.isNative()) b.fmt(
            "target/{s}/{s}/libicu_capi.a",
            .{ rust_target, if (release) "release" else "debug" },
        ) else b.fmt(
            "target/{s}/libicu_capi.a",
            .{if (release) "release" else "debug"},
        ),
    );
    // b.installLibFile() calls b.path() which expects a relative path - so we have to do this manually
    const install_lib = b.addInstallFileWithDir(
        libicu_capi,
        .lib,
        "libicu_capi.a",
    );
    install_lib.step.dependOn(&run_cargo.step);
    b.getInstallStep().dependOn(&install_lib.step);

    const install_headers = b.addInstallDirectory(.{
        .source_dir = icu4x.path("ffi/capi/bindings/c"),
        .install_dir = .header,
        .install_subdir = "",
    });
    install_headers.step.dependOn(&run_cargo.step);
    b.getInstallStep().dependOn(&install_headers.step);
}
