# icu4x built with Zig

This project builds the
[icu4x C API](https://github.com/unicode-org/icu4x/tree/main/ffi/capi) with Zig.
These are _not Zig language bindings_ to the project. The goal of this project
is to enable the upstream project to be used with the Zig package manager.
Downstream users may also not be Zig language users, they may just be using Zig
as a build system.

## Usage

Install cargo and rustc.

Create a `build.zig.zon` like so:

```zig
.{
    .name = "my-project",
    .version = "0.0.0",
    .dependencies = .{
        .icu4x = .{
            .url = "https://codeberg.org/linusg/zig-build-icu4x/archive/<git-ref-here>.tar.gz",
            .hash = "12208070233b17de6be05e32af096a6760682b48598323234824def41789e993432c",
        },
    },
}
```

And in your `build.zig`:

```zig
const icu4x = b.dependency("icu4x", .{ .target = target, .optimize = optimize });
const exe = b.addExecutable(...);
exe.step.dependOn(icu4x.builder.getInstallStep());
exe.addObjectFile(.{
    .path = icu4x.builder.getInstallPath(.lib, "libicu_capi.a"),
});
exe.addIncludePath(.{
    .path = icu4x.builder.getInstallPath(.header, ""),
});
```

In your code you can now `@cImport` the project.
